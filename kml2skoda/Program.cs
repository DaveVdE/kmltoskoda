﻿using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace kml2skoda
{
    class Program
    {
        static void Main(string[] args)
        {
            var source = args.ElementAtOrDefault(0);
            var destination = args.ElementAtOrDefault(1) ?? Path.ChangeExtension(source, ".vcf");

            var kml = XNamespace.Get("http://www.opengis.net/kml/2.2");
            var document = XDocument.Load(source);
            var inner = document.Root.Element(kml + "Document");
            var elements = document.Root.Elements();

            using (var writer = new StreamWriter(destination, false, Encoding.Default))
            {
                var placemarks = inner.Descendants(kml + "Placemark").Select(placemarkElement => new
                {
                    Name = (string) placemarkElement.Element(kml + "name"),
                    Coordinates = (string) placemarkElement.Element(kml + "Point").Element(kml + "coordinates")
                });

                foreach (var placemark in placemarks)
                {
                    var coordinates = string.Join(";", placemark.Coordinates.Split(',').Take(2).Reverse().Select(coordinate =>
                    {
                        // Too many digits for the poor computer to interpret.
                        var asFloat = float.Parse(coordinate, CultureInfo.InvariantCulture);
                        return asFloat.ToString(CultureInfo.InvariantCulture);
                    }));
                   
                    writer.WriteLine("BEGIN:VCARD");
                    writer.WriteLine("VERSION:3.0");
                    writer.WriteLine("FN:{0}", placemark.Name);
                    writer.WriteLine("N:{0}", placemark.Name);
                    writer.WriteLine("GEO:{0}", coordinates);
                    writer.WriteLine("END:VCARD");
                }
            }
        }
    }
}
